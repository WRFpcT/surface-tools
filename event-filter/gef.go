// gef : go event filter

package main

import (
	"encoding/xml"
	"os"
	"io"
	"fmt"
	"time"
	"strings"
)

// XML parsing is trial an error with magic strings. if something is wrong parser fails silently.
// https://pkg.go.dev/encoding/xml

type Provider struct {
	Name string `xml:"Name,attr"`
}

type TimeCreated struct {
	SystemTime string `xml:"SystemTime,attr"`
}

type Execution struct {
	ProcessID int `xml:"ProcessID,attr"`
	ThreadID int `xml:"ThreadID,attr"`
}

type System struct {
	Provider Provider
	Id int `xml:"EventID"`
	Level int
	// this does not work, examples show access to nested entities but not attributes of nested entities
	// TimeCreated string `xml:"TimeCreated>SystemTime,attr"`
	//
	// solution: create nested structs (e.g. TimeCreated)
	// note: this is crazy, adding the bad "xml:..." will silently break parsing of other fields
	TimeCreated TimeCreated
	Execution Execution
}

type Data struct {
	Name string `xml:"Name,attr"`
	Value string `xml:",chardata"`
}
type EventData struct {
	Data []Data
	Binary string
}

type RenderingInfo struct {
	Message string
}

type Event struct {
	System System
	EventData EventData
	RenderingInfo RenderingInfo
}

type PrintFormat struct {
	hasWhite bool
	white []string
	hasBlack bool
	black []string
}

func main() {
	var pf PrintFormat
	for _, arg := range os.Args {
		if strings.HasPrefix(arg, "EDW:") {
			pf.hasWhite = true
			pf.white = getList(arg)
		}
		if strings.HasPrefix(arg, "EDB:") {
			pf.hasBlack = true
			pf.black = getList(arg)
		}
	}

	decoder := xml.NewDecoder(os.Stdin) // what encoding? UTF-8 only?
	for {
		// Read tokens from the XML document in a stream. 
		t, err := decoder.Token()
		if err == io.EOF {
			break
		}
		doErr(err)
		switch se := t.(type) {
			case xml.StartElement:
				if se.Name.Local == "Event" {
					var ev Event
					decoder.DecodeElement(&ev, &se)
					print(ev, pf)
				}
		}
	}
}

func matchList(list []string, name string) bool {
	for _, entry := range list {
		if strings.Contains(name, entry) {
			return true
		}
	}
	return false
}

func getList(arg string) []string {
	if len(arg) == 4 {
		return []string{} // list is "empty", split would return single zero length string
	}
	return strings.Split(arg[4:], ",")
}

func print(e Event, pf PrintFormat) {
	date, err := time.Parse(time.RFC3339, e.System.TimeCreated.SystemTime)
	doErr(err)

	fmt.Println(
		date.Local().Format("2006-01-02 15:04:05.000"), // local time with millis
		printLevel(e.System.Level),
		printSource(e.System.Provider.Name),
		e.System.Id,
		printMessage(e.RenderingInfo.Message),
		printData(e.EventData, pf) )
	// ) // go compiler: must not place this closing ")" in this line, this is SEMANTIC WHITESPACE!
}

func printData(data EventData, pf PrintFormat) string {
	var b strings.Builder
	for i, d := range data.Data {
		if skip(pf, d.Name) {
			continue
		}
		name := d.Name
		if len(name) == 0 {
			name = fmt.Sprintf("Data%d", i)
		}
		fmt.Fprintf(&b, "%s=%s ", name, d.Value)
	}
	if len(data.Binary) > 0 && !skip(pf, "Binary") {
		fmt.Fprintf(&b, "Binary=%s", data.Binary)
	}
	return b.String()
}

func skip(pf PrintFormat, name string) bool {
	if pf.hasWhite && !matchList(pf.white, name) {
		return true
	}
	if pf.hasBlack && matchList(pf.black, name) {
		return true
	}
	return false
}

func printMessage(source string) string {
	msg1 := strings.TrimSuffix(source, "\n")
	if !strings.Contains(msg1, "\n") {
		return msg1
	}
	if (len(msg1) < 120) {
		return strings.ReplaceAll(msg1, "\n", "")
	}
	return msg1
}

func printSource(source string) string {
	return strings.TrimPrefix(strings.TrimPrefix(source, "Microsoft-Windows-"), "Microsoft.Windows.")
}

func printLevel(level int) string {
	switch level {
		case 0: return "I"
		case 1: return "C"
		case 2: return "E"
		case 3: return "W"
		case 4: return "I"
		case 5: return "V"
	}
	return fmt.Sprintf("ERROR: unknown level %d", level)
}

func doErr(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}