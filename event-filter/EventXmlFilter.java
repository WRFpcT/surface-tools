import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

/**
 * A little tool to format output of Windows Eventing Command Line Utility "wevtutil.exe /f:RenderedXml".
 */
public class EventXmlFilter {

    public static void main(String[] args) throws Exception {

        ArrayList<String> whiteList = null;
        ArrayList<String> blackList = null;

        for (String arg : args) {
            // comma seperated whitelist for event data names
            if (arg.toUpperCase().startsWith("EDW:")) {
                whiteList = getList(arg.substring(4));
            }
            // comma seperated blacklist for event data names
            if (arg.toUpperCase().startsWith("EDB:")) {
                blackList = getList(arg.substring(4));
            }
        }

        // trick: use output encoding for input stream
        String encoding = System.getProperty("file.encoding");  // got "cp1252";
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in, encoding));

        // using tedious DOM API. it sucks but is integrated in JDK
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

        String line = null;
        String xml = "";
        while ((line = input.readLine()) != null) {
            if (line.contains("Events>")) { // ignore outer <Events> ... </Events> as generated by tracerpt
                continue;
            }
            xml += line + "\n";
            if (!xml.contains("</Event>")) {
                // read another line
                continue;
            }
            // hack fix funny quotes that don't exist in cp850 and cp437 (only in cp1252).
            String consoleEncoding = System.getProperty("sun.stdout.encoding");
            if (consoleEncoding != null && !consoleEncoding.equals("cp1252")) {
                xml = xml.replaceAll("\u201E", "\""); // „
                xml = xml.replaceAll("\u201C", "\""); // “
            }
            Document document = builder.parse(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));

            String level = getXmlValue(document, "Level", null);
            String time = getXmlValue(document, "TimeCreated", "SystemTime");
            String source = getXmlValue(document, "Provider", "Name");
            String prefix = "Microsoft-Windows-";
            source = source.startsWith(prefix) ? source.substring(prefix.length()) : source;
            prefix = "Microsoft.Windows.";
            source = source.startsWith(prefix) ? source.substring(prefix.length()) : source;
            // requires format /f:RenderedXml
            String message = getXmlValue(document, "Message", null);
            String id = getXmlValue(document, "EventID", null);
            String data = getEventData(document, whiteList, blackList);

            System.out.print(getDate(time) + " " +
                    getLevel(level) + " " +
                    source + " " +
                    id + " " +
                    formatMessage(message) + " " +
                    data + "\n");
            // reset
            xml = "";
        }
    }

    private static String formatMessage(String message) {
        // remove final "\n".
        if (message.length() > 0 && message.charAt(message.length() - 1) == '\n') {
            // cut away last new line.
            message = message.substring(0, message.length() - 1);
        }
        // single line message?
        if (!message.contains("\n")) {
            return message;
        }
        // for short message avoid (pointless) new lines.
        if (message.length() < 120) {
           return message.replaceAll("\n", " ");
        }
        // format long message as block
        return "\n    " + message.replaceAll("\n", "\n    ") + "\n";
    }

    private static String getXmlValue(Document document, String element, String attribute) {
        NodeList list = document.getElementsByTagName(element);
        if (list.getLength() == 0) {
            return "";  // "not found"
        }
        Element value = (Element) list.item(0);
        if (attribute == null) {
            return value.getTextContent();
        }
        return value.getAttribute(attribute);
    }

    private static String getEventData(Document document, ArrayList<String> whiteList, ArrayList<String> blackList) {
        NodeList list = document.getElementsByTagName("EventData");
        if (list.getLength() == 0) {
            return "";  // "not found"
        }
        Element value = (Element) list.item(0);
        // iterate all <Data> children
        NodeList dataList = value.getChildNodes();
        String ret = "";
        for (int i = 0; i < dataList.getLength(); i++) {
            Object child = dataList.item(i);
            if (!(child instanceof Element)) {
                continue; // why?
            }
            Element data = (Element) child;
            String name = data.getAttribute("Name");
            if (name.length() == 0) {
                name = "Data" + i;  // for data without name.
            }
            // whitelist first
            if (whiteList != null && !match(whiteList, name)) {
                continue; // skip: not found in whitelist
            }
            // then check blacklist
            if (blackList != null && match(blackList, name)) {
                continue; // skip: found in blacklist
            }

            ret += name + "=" + formatMessage(data.getTextContent().trim()) + " ";
        }
        return ret;
    }

    // https://learn.microsoft.com/en-us/dotnet/api/system.diagnostics.tracing.eventlevel?view=net-7.0
    private static String getLevel(String val) {
        if (val.equals("0")) return "I";  // "Always": log as Info
        if (val.equals("1")) return "C";
        if (val.equals("2")) return "E";
        if (val.equals("3")) return "W";
        if (val.equals("4")) return "I";
        if (val.equals("5")) return "V";
        throw new RuntimeException("unknown level: " + val);
    }

    private static String getDate(String date) throws Exception {
        // fixing time zone
        // substring(0, 23): crop nanoseconds and Z
        // append +0000 as time zone that SimpleDateFormat understands
        Date date2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").parse(date.substring(0, 23) + "+0000");
        // format as "local" time
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(date2);
    }

    private static ArrayList<String> getList(String arg) {
        ArrayList<String> ret = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(arg, ",");
        while (st.hasMoreTokens()) {
            ret.add(st.nextToken());
        }
        return ret;
    }

    private static boolean match(ArrayList<String> list, String name) {
        for (String entry : list) {
            if (name.contains(entry)) {
                return true;
            }
        }
        return false;
    }
}