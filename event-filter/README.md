# event-filter

This tool EventFilter converts the xml output of `wevtutil` to something readable.

## event-filter in go

go event filter (gef) is the go variant.

compile with `go build gef.go`.

Note: the go variant is a full replacement of jef with some bug fixes, jef is considered legacy.

## event-filter in java (legacy)

JavaEventFilter (jef) is the java variant.

compile code:
`javac EventXmlFilter.java`

use like this:
`wevtutil qe System /f:RenderedXml /rd:true  | java EventXmlFilter`

optional create `jef.bat` 
```
@echo off
<path to java>\java.exe -cp <path to class> EventXmlFilter %*
```

added `jef.exe` binary compiled with graal `native-image EventXmlFilter`.

added command line options `EDW:` and `EDB:` as white list and black list for event data. see according samples.

sample command lines:
```
wevtutil qe System "/q:*[System [Provider[@Name='Microsoft-Windows-Kernel-Power' or @Name='Microsoft-Windows-Power-Troubleshooter']]]" /c:20 /f:RenderedXml /rd:true | jef EDW:Reason,State,AcOnline,BatteryCount,Duration EDB:ExternalMonitor
wevtutil qe System "/q:*[System [Provider[@Name='Microsoft-Windows-Kernel-General'] and (EventID=12 or EventID=13)]]" /f:RenderedXml /rd:true /c:10 | jef 
wevtutil qe System "/q:*[System [(Level=2)]]" /f:RenderedXml /rd:true /c:20 | jef
wevtutil qe Microsoft-Windows-WLAN-AutoConfig/Operational "/q:*[System [Task = 24010]]" /c:20 /rd:true | jef EDW:ConnectionId,Reason,Val
wevtutil qe Microsoft-Windows-TaskScheduler/Operational "/q:*[EventData[Data[@Name='TaskName']='\Microsoft\Windows\Customer Experience Improvement Program\UsbCeip']]" /c:20 /f:RenderedXml /rd:true | jef EDW:
wevtutil qe Microsoft-Windows-TaskScheduler/Operational "/q:*[System [EventID=140]]" /c:20 /f:RenderedXml /rd:true | jef EDW:
```

tracerpt sample (can only output to file, this is clumsy)
```
tracerpt C:\Windows\Logs\waasmedic\waasmedic.*.etl -o %TEMP%\trace1234.tmp -y
type %TEMP%\trace1234.tmp | jef
del %TEMP%\trace1234.tmp
```

example output:
```
wevtutil qe System "/q:*[System [Provider[@Name='SurfaceHotPlug'] and TimeCreated[timediff(@SystemTime) <= 86400000]]]"  /f:RenderedXml /rd:true | jef
2023-02-18 14:05:22.275 I SurfaceHotPlug 2 Base state change: BaseAttached=1 Data 0:  Data 1: BaseAttached=1 Data 2: 00000000020030000000000002000140000000000000000000000000000000000000000000000000
2023-02-18 13:54:45.323 I SurfaceHotPlug 3 DGPU state change: DGPUPresent=1 Data 0:  Data 1: DGPUPresent=1 Data 2: 00000000020030000000000003000140000000000000000000000000000000000000000000000000
2023-02-18 13:54:45.271 I SurfaceHotPlug 2 Base state change: BaseAttached=1 Data 0:  Data 1: BaseAttached=1 Data 2: 00000000020030000000000002000140000000000000000000000000000000000000000000000000
2023-02-18 13:54:39.644 I SurfaceHotPlug 2 Base state change: BaseAttached=0 Data 0:  Data 1: BaseAttached=0 Data 2: 00000000020030000000000002000140000000000000000000000000000000000000000000000000
2023-02-18 13:54:39.644 I SurfaceHotPlug 3 DGPU state change: DGPUPresent=0 Data 0:  Data 1: DGPUPresent=0 Data 2: 00000000020030000000000003000140000000000000000000000000000000000000000000000000
```

