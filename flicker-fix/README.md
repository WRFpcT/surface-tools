# flicker-fix

## the issue

Surface Book 1 and 2 (at least) have the issue that sometimes the top row of pixels is flickering, this was never fixed (tested last driver update August 2022 for Surface Book 2).

There is a clumsy work-around that increases battery drain, you need to install a different intel driver and disable Panel Self-Refresh (PSR).

some related links:
https://answers.microsoft.com/en-us/surface/forum/surfbook2-surfdrivers/surface-book-2-top-line-screen-flicker/653d2920-9c77-4334-b441-f2ca7e85b9dc
https://answers.microsoft.com/en-us/windows/forum/windows_10-performance-winpc/surface-book-top-edge-flicker/f7a32f3a-e980-4d7a-9172-7fd4c0f0c729
https://answers.microsoft.com/de-de/surface/forum/surfbook-surfdrivers/oberer-displayrand-flackert-im-akkubetrieb/5bb991db-7f62-4bea-acc0-13531ffdb547
https://answers.microsoft.com/en-us/surface/forum/all/surface-book-2-top-line-of-the-screen-flickering/30282582-ac73-408b-be63-2d9b9678d408

## my fix

This tool draws a black non decorated "always on top" windows with 3000x1 pixels. This makes the flickering invisible.

Minor downside: Mouse clicks on top row be "swallowed" (just klick one pixel below).

## details

The trivial autohotkey script `flickerFix.ahk` was converted to `flickerFix.exe` with `Ahk2Exe.exe`.

Copy or link to `%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup` for autostart.