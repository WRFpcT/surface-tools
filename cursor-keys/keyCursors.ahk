#NoTrayIcon

; ----------------------------------
; use right shift as extra alterator
; ----------------------------------

RShift & Down:: Send {PgDn}
return

RShift & Up:: Send {PgUp}
return

RShift & Left:: Send {Home}
return

RShift & Right:: Send {End}
return

; -- no insert when fn is active: convert F12 to insert key ----
F12:: Send {Insert}
return

