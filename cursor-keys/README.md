# cursor-keys

This tool converts the cursor keys into home-end-pgup-pgdown when right shift is pressed, so you can use these with one hand.
The idea comes from an ancient Sony Vaio (series 505) with a second FN key close to the (non crippled!) cursor keys.
(It also converts F12 to Insert, as I need this frequently).


## details

The trivial `keyCursors.ahk` was converted to `keyCursors.exe` with `Ahk2Exe.exe`.

Copy or link to `%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup` for autostart.